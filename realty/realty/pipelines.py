# coding: utf-8

from advert.models import Realty

import datetime

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html

class RealtyPipeline(object):

    def __init__(self, *args, **kwargs):
        super(RealtyPipeline, self).__init__(*args, **kwargs)
        self._import_date = datetime.datetime.now()

    def process_item(self, item, spider):
        # r = Realty(**item)
        # r.date = self._import_date
        # r.save()
        item['date'] = self._import_date
        item.save()
        return item
