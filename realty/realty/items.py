# coding: utf-8
# http://doc.scrapy.org/en/latest/topics/items.html

from scrapy.item import Item, Field

from scrapy.contrib.djangoitem import DjangoItem

from advert.models import Realty


class RealtyItem(DjangoItem):

    django_model = Realty
