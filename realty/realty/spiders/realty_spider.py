# coding: utf-8
from django.conf import settings

from scrapy.spider import Spider
from scrapy.selector import Selector, HtmlXPathSelector
from scrapy.http import Request

from realty.items import RealtyItem

from advert.models import Realty

import urllib
import re
import os

BASE_URL = 'dom.72.ru'
URL = 'http://%s' % BASE_URL


class RealtySpider(Spider):

    name = "realty"
    allowed_domains = [BASE_URL]

    def __init__(self, *args, **kwargs):
        super(RealtySpider, self).__init__(*args, **kwargs)
        Realty.objects.all().delete()
        self.start_urls = []
        for x in xrange(1,259):
            part1 = '%s/realty/sell/residential/new/#%d.php' % (URL, x)
            part2 = '%order=DateUpdate&dir=desc&PriceUnit=1&AreaUnit=1&expand=0&PriceUnit=1'
            self.start_urls.append(
                '%s%s' % (part1, part2),
            )
        self.path = os.path.abspath(os.path.dirname(os.path.dirname(os.path.dirname(__file__))))

    def parse(self, response):
        sel = Selector(response)
        # найдем ссылки на конкретные объявления
        rows = sel.css('table[class=adv_table] tr[valign!=top]')
        # отчистим найденное от мусора
        for i, row in enumerate(rows):
            hrefs = row.css('td:nth-child(2) a::attr(href)').extract()
            hrefs = filter(lambda x: x.startswith('/realty'), hrefs)
            try:
                href = hrefs[0]
            except IndexError:
                href = None
            else:
                item = RealtyItem()
                item['address'] = ''.join(
                            row.css('td:nth-child(2) a b::text').extract()).strip()
                item['url'] = href.strip()
                try:
                    item['note'] = row.css('div[class=rl_note]::text').extract()[0].strip()
                except IndexError:
                    item['note'] = ''
                blacks = row.css('td[class=black]::text').extract()
                try:
                    item['price'] = blacks[0].strip()
                except IndexError:
                    pass
                else:
                    try:
                        item['area'] = blacks[1].strip()
                    except IndexError:
                        pass
                    else:
                        try:
                            item['rooms'] = blacks[2].strip()
                        except IndexError:
                            pass
                        else:
                            try:
                                item['floor'] = blacks[3].strip()
                            except IndexError:
                                pass
                # image
                if getattr(settings, 'IMPORT_IMAGE', False):
                    try:
                        src = row.css('td[align=center] a img::attr(src)').extract()[0]
                    except IndexError:
                        pass
                    else:
                        new_slug = 'realty%d.jpg' % i
                        new_path = os.path.join(self.path, settings.MEDIA_ROOT, new_slug)
                        urllib.urlretrieve(src, new_path)
                        item['img'] = new_slug

                # contacts
                item['contacts'] = '; '.join(filter(
                    lambda x: x.strip(), row.css('td:last-child::text').extract()))
                yield item
