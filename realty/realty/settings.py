# Scrapy settings for realty project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#
import os
import sys

path = os.path.abspath(os.path.dirname(os.path.dirname(os.path.dirname(__file__))))
sys.path.append(path)

# Setting up django's settings module name.
# This module is located at /home/rolando/projects/myweb/myweb/settings.py.
os.environ['DJANGO_SETTINGS_MODULE'] = 'arttest.settings'


BOT_NAME = 'realty'

SPIDER_MODULES = ['realty.spiders']
NEWSPIDER_MODULE = 'realty.spiders'

# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'realty (+http://www.yourdomain.com)'

ITEM_PIPELINES = [
    'realty.pipelines.RealtyPipeline',
]


IMPORT_IMAGE = False