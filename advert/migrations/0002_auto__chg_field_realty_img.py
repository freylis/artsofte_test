# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'Realty.img'
        db.alter_column(u'advert_realty', 'img', self.gf('django.db.models.fields.files.ImageField')(max_length=255, null=True))

    def backwards(self, orm):

        # Changing field 'Realty.img'
        db.alter_column(u'advert_realty', 'img', self.gf('django.db.models.fields.CharField')(max_length=255, null=True))

    models = {
        u'advert.realty': {
            'Meta': {'object_name': 'Realty'},
            'address': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'area': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'contacts': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'date': ('django.db.models.fields.DateTimeField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            'floor': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'img': ('django.db.models.fields.files.ImageField', [], {'max_length': '255', 'null': 'True'}),
            'note': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'price': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'rooms': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True'})
        }
    }

    complete_apps = ['advert']