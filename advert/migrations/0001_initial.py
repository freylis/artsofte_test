# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Realty'
        db.create_table(u'advert_realty', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('address', self.gf('django.db.models.fields.CharField')(max_length=255, null=True)),
            ('url', self.gf('django.db.models.fields.URLField')(max_length=200, null=True)),
            ('note', self.gf('django.db.models.fields.CharField')(max_length=255, null=True)),
            ('price', self.gf('django.db.models.fields.CharField')(max_length=255, null=True)),
            ('area', self.gf('django.db.models.fields.CharField')(max_length=255, null=True)),
            ('rooms', self.gf('django.db.models.fields.CharField')(max_length=255, null=True)),
            ('floor', self.gf('django.db.models.fields.CharField')(max_length=255, null=True)),
            ('contacts', self.gf('django.db.models.fields.CharField')(max_length=255, null=True)),
            ('img', self.gf('django.db.models.fields.CharField')(max_length=255, null=True)),
            ('date', self.gf('django.db.models.fields.DateTimeField')(default=None, null=True, blank=True)),
        ))
        db.send_create_signal(u'advert', ['Realty'])


    def backwards(self, orm):
        # Deleting model 'Realty'
        db.delete_table(u'advert_realty')


    models = {
        u'advert.realty': {
            'Meta': {'object_name': 'Realty'},
            'address': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'area': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'contacts': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'date': ('django.db.models.fields.DateTimeField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            'floor': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'img': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'note': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'price': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'rooms': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True'})
        }
    }

    complete_apps = ['advert']