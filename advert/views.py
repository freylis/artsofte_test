# coding: utf-8

from django.shortcuts import render
from django.views.generic import ListView
from django.views.generic.list import MultipleObjectMixin

from advert.models import Realty


class RealtyListView(ListView, MultipleObjectMixin):

    model = Realty
    paginate_by = 20
