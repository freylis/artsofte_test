# coding: utf-8

from django.db import models


class Realty(models.Model):

    class Meta:
        verbose_name = u'Недвижимость'
        verbose_name_plural = u'Недвижимость'

    address = models.CharField(u'Адрес', null=True, blank=False, max_length=255)
    url = models.URLField(u'Ссылка', null=True, blank=False)
    note = models.CharField(u'Примечание', null=True, blank=False, max_length=255)
    price = models.CharField(u'Цена', null=True, blank=False, max_length=255)
    area = models.CharField(u'Площадь', null=True, blank=False, max_length=255)
    rooms = models.CharField(u'Кол-во комнат', null=True, blank=False, max_length=255)
    floor = models.CharField(u'Этаж', null=True, blank=False, max_length=255)
    contacts = models.CharField(u'Контакты', null=True, blank=False, max_length=255)
    img = models.ImageField(u'Изображение', null=True, blank=False, max_length=255,
                                                                upload_to='items/')
    date = models.DateTimeField(u'Дата импорта', null=True, blank=True, default=None,
                                                                        editable=False)

    def __unicode__(self):
        return self.address

    def get_fields(self):
        result = []
        fields = self._meta.fields
        for field in fields:
            if field.name not in ['img', 'url']:
                value = getattr(self, field.name)
                if value:
                    result.append({
                            'label': field.verbose_name,
                            'value': value,
                        })
        return result
